<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>  
    <script src=" https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>  

</head>
<body>
<table id ="reportTable" border="1"  class="table" width="100%" >
  <thead>
  <tr bgcolor="#A8A8A8">
        <th colspan="3">Report Type</th>
        <th  colspan="3" >Students</th>
        <th colspan="3" > Halaqat</th>
        <th  colspan="3" >from</th>
        <th colspan="2" >to</th>
</tr>
    <tr>
        <th colspan="3">Attendance</th>
        <th  colspan="3" >All Students</th>
        <th colspan="3" >All Halaqat</th>
        <th  colspan="3" >1-1-2019</th>
        <th colspan="2" >11-12-2019</th>
  </tr>
  
    <tr bgcolor = "#A8A8A8">
      <th rowspan="2">Halaqat</th>
      <th rowspan="2" >National ID</th>
      <th rowspan="2" >student Name</th>
      <th colspan="4">Attendance  </th> 
      <th  colspan="3">Evaluation  </th>
      <th  colspan="4">Orders  </th>
    </tr>
    <tr>
    <th>attendant</th>
    <th>absent</th>
    <th>apologize</th>
    <th>late</th>
     
    <th>points</th>
    <th>percent</th>
    <th>level</th>

    <th>Memorization</th>
    <th>center</th>
    <th>Association</th>
    <th>Circle</th>
     
    </tr>
  </thead>

  <tbody>
  
  </tbody>
</table>

<script>
$(document).ready( function () {
    $('#reportTable').DataTable({
      ajax: './ReportData.php',
      columns :[
          {"data":"0"},
          {"data":"1"},
          {"data":"2"},
          {"data":"3"},
          {"data":"4"},
          {"data":"5"},
          {"data":"6"},
          {"data":"7"},
          {"data":"8"},
          {"data":"9"},
          {"data":"10"},
          {"data":"11"},
          {"data":"12"},
          {"data":"13"}
      ]   
      //  ordering:  false
    });
} );
</script>
</body>
</html>